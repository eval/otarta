SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
.PHONY: clean test set-pom-version

dev:
	npx shadow-cljs -A:user/dev:dev watch lib

compile: public/js/core.js

test:
	clojure -Atest

test-watch:
	clojure -Atest-watch

public/js/core.js:
	npx shadow-cljs -A:dev release lib

clean:
	mvn ${MAVEN_CLI_OPTS} clean
	rm -rf target cljs-test-runner-out public/js

mvn-deploy: set-pom-version set-pom-tag clean
	mvn ${MAVEN_CLI_OPTS} deploy

mvn-install: set-pom-version set-pom-tag clean
	mvn ${MAVEN_CLI_OPTS} install

set-pom-version:
	# replacing only first instance
	sed -i "0,/<version>.*<\/version>/{s;;<version>$${version}</version>;}" pom.xml

set-pom-tag:
	sed -i "s;<tag>TAG</tag>;<tag>$${tag}</tag>;" pom.xml
